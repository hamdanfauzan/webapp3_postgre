﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapp3_postgre.DataProviders;
using webapp3_postgre.Models;

namespace webapp3_postgre.BusinessProviders
{
    public class BlogBusiness
    {
        private readonly IConfiguration Configuration;

        public BlogBusiness(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<IEnumerable<Blog>> GetBlog(int id = 0)
        {
            BlogData _blogData = new BlogData(Configuration);
            string _id = id == 0 ? string.Empty : id.ToString();
            IEnumerable<Blog> ieBlog = await _blogData.GetBlog(_id);

            return ieBlog;
        }

        public async Task<int> InsertBlog(string title, string description)
        {
            BlogData _blogData = new BlogData(Configuration);
            int id = 0;
            string _id = id.ToString();
            await _blogData.InsertBlog(_id, title, description);
            return id;
        }

        public async Task UpdateBlog(int id, string title, string description)
        {
            BlogData _blogData = new BlogData(Configuration);
            string _id = id.ToString();
            await _blogData.UpdateBlog(_id, title, description);
        }

        public async Task DeleteBlog(int id)
        {
            BlogData _blogData = new BlogData(Configuration);
            string _id = id.ToString();
            await _blogData.DeleteBlog(_id);
        }

    }
}
