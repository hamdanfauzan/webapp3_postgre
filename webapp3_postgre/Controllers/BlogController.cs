﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapp3_postgre.BusinessProviders;
using webapp3_postgre.Models;

namespace webapp3_postgre.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[EnableCors("AllowOrigin")]
    public class BlogController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public BlogController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                BlogBusiness _blogBusiness = new BlogBusiness(Configuration);
                IEnumerable<Blog> ieBlog = await _blogBusiness.GetBlog();
                if (ieBlog == null)
                    return NotFound();

                return Ok(ieBlog);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return StatusCode(500, ex.Message);
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetByID(string id)
        {
            try
            {
                BlogBusiness _blogBusiness = new BlogBusiness(Configuration);
                int _id = Convert.ToInt32(id);
                IEnumerable<Blog> ieBlog = await _blogBusiness.GetBlog(_id);
                if (ieBlog == null)
                    return NotFound();

                return Ok(ieBlog);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return StatusCode(500, ex.Message);
            }

        }

        [HttpPost]
        public async Task<ObjectResult> Create([FromBody] Blog blog)
        {
            ObjectResult response = new ObjectResult(null);
            Notif d = new Notif();
            try
            {
                BlogBusiness _blogBusiness = new BlogBusiness(Configuration);
                await _blogBusiness.InsertBlog(blog.Title, blog.Description);

                d.Type = "success";
                d.Head = "Success";
                d.Notes = "Success to create data";
                //d.Obj = new { ID = newID };
                response = StatusCode(201, d);

            }
            catch (Exception ex)
            {
                d.Type = "error";
                d.Head = "Failed";
                d.Notes = ex.Message;
                response = StatusCode(500, d);
            }
            return response;
        }

        [HttpPost]
        public async Task<ObjectResult> Update([FromBody] Blog blog)
        {
            ObjectResult response = new ObjectResult(null);
            Notif d = new Notif();
            try
            {
                BlogBusiness _blogBusiness = new BlogBusiness(Configuration);
                int _id = Convert.ToInt32(blog.Id);

                await _blogBusiness.UpdateBlog(_id, blog.Title, blog.Description);

                d.Type = "success";
                d.Head = "Success";
                d.Notes = "Success to update data";
                d.Obj = new { ID = _id };
                response = StatusCode(201, d);
            }
            catch (Exception ex)
            {
                d.Type = "error";
                d.Head = "Failed";
                d.Notes = ex.Message;
                response = StatusCode(500, d);
            }
            return response;
        }

        [HttpPost]
        public async Task<ObjectResult> Delete([FromBody] Blog blog)
        {
            ObjectResult response = new ObjectResult(null);
            Notif d = new Notif();
            try
            {
                BlogBusiness _blogBusiness = new BlogBusiness(Configuration);
                int _id = Convert.ToInt32(blog.Id);

                await _blogBusiness.DeleteBlog(_id);

                d.Type = "success";
                d.Head = "Success";
                d.Notes = "Success to delete data";
                response = StatusCode(201, d);
            }
            catch (Exception ex)
            {
                d.Type = "error";
                d.Head = "Failed";
                d.Notes = ex.Message;
                response = StatusCode(500, d);
            }
            return response;
        }
    }
}
