﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using webapp3_postgre.Models;

namespace webapp3_postgre.DataProviders
{
    public class BlogData
    {
        private readonly IConfiguration Configuration;

        public BlogData(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<IEnumerable<Blog>> GetBlog(string id = "")
        {
            var dbString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            using (var sqlConnection = new NpgsqlConnection(dbString))
            {
                string errorMessage = string.Empty;
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();

                dynamicParameters.Add("vId", id);
                //dynamicParameters.Add("vMessageText", errorMessage);

                IEnumerable blog;
                if (id == string.Empty)
                {
                    blog = sqlConnection.Query("select * from blog");

                }
                else
                {
                    blog = sqlConnection.Query("select * from blog where id = " + id + "order by id");
                }
                List<Blog> lsBlog = new List<Blog>();
                foreach (var item in blog)
                {
                    var castItem = (IDictionary<string, object>)item;
                    Blog obj = new Blog();
                    obj.Id = castItem["Id"].ToString();
                    obj.Title = castItem["Title"] == null ? string.Empty : castItem["Title"].ToString();
                    obj.Description = castItem["Description"] == null ? string.Empty : castItem["Description"].ToString();

                    lsBlog.Add(obj);
                }

                return lsBlog;
            }
        }

        public async Task InsertBlog(string id, string title, string description)
        {
            var dbString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            using (var sqlConnection = new NpgsqlConnection(dbString))
            {
                string errorMessage = string.Empty;
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();

                dynamicParameters.Add("vTitle", title);
                dynamicParameters.Add("vDescription", description);

                //dynamicParameters.Add("vMessageText", errorMessage);

                //await sqlConnection.ExecuteScalarAsync<string>("usp_InsertBlog", dynamicParameters, commandType: CommandType.StoredProcedure);
                await sqlConnection.ExecuteScalarAsync<string>("insert into blog (\"Title\", \"Description\") values('" + title + "', '" + description + "');");

            }
        }

        public async Task UpdateBlog(string id, string title, string description)
        {
            var dbString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            using (var sqlConnection = new NpgsqlConnection(dbString))
            {
                string errorMessage = string.Empty;
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();

                dynamicParameters.Add("vId", id);
                dynamicParameters.Add("vTitle", title);
                dynamicParameters.Add("vDescription", description);

                //dynamicParameters.Add("vMessageText", errorMessage);

                await sqlConnection.ExecuteScalarAsync<string>("usp_UpdateBlog", dynamicParameters, commandType: CommandType.StoredProcedure);

            }
        }

        public async Task DeleteBlog(string id)
        {
            var dbString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            using (var sqlConnection = new NpgsqlConnection(dbString))
            {
                string errorMessage = string.Empty;
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();

                dynamicParameters.Add("vId", id);

                //dynamicParameters.Add("vMessageText", errorMessage);

                await sqlConnection.ExecuteScalarAsync<string>("usp_DeleteBlog", dynamicParameters, commandType: CommandType.StoredProcedure);

            }
        }
    }
}
