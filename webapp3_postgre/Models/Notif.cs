﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapp3_postgre.Models
{
    public class Notif
    {
        public string Type { set; get; }
        public string Head { set; get; }
        public string Notes { set; get; }
        public object Obj { set; get; }
    }
}
