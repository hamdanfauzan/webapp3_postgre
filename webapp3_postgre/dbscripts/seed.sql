
\connect blogdb

CREATE TABLE blog
(
    "Id" serial PRIMARY KEY,
    "Title"  VARCHAR (50)  NOT NULL,
    "Description"  VARCHAR (100)  NOT NULL
);
CREATE ROLE bloguser;
ALTER TABLE "blog" OWNER TO bloguser;

Insert into blog("Title","Description") values( 'Title 1','Description 1');
Insert into blog("Title","Description") values( 'Title 2','Description 2');
Insert into blog("Title","Description") values( 'Title 3','Description 3');
Insert into blog("Title","Description") values( 'Title 4','Description 4');
